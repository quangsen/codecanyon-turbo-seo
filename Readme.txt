Turbo SEO Analyzer:

Turbo SEO Analyzer is a website review and SEO tool. That allow you to collect different SEO statistics like Alexa rank, Google page rank,
WHOIS info and various social networking statistics etc.. It comes with a very clean design and ajax based request system. The reports can
be viewed not only on the site, but they can also be downloaded as PDF file.


Features
- Generate report as PDF file
- Social stats checker
- Various ranking stats such Alexa rank, Google page rank etc..
- Domain age with WHOIS info
- Domain IP blacklist checker
- Recent sites for visitors
- Website screen preview
- Host information with ISP
- Offline / Online checker
- Average load time of site
- Score board (Out of 100 - Easy to measure the worth of site) 
- Optimize suggestion for meta tags
- 100% Real time data only (No cached)
- 95% AJAX based request only
- Contact page for visitors to contact you easily
- Powerfull admin control panel
- Clean and responsive design layout
- One-Click Ads integration with responsive design support
� Fully translatable to any language
- Robots.txt and Sitemap checker included
- gTLD whois support
- Google Safe Browsing checker.
- Website antivirus checker (Identifying potentially harmful sites)
- Share IT widget on Result page.
- XML Sitemap generator.
- Fully SEO friendly URL.
- and many more included....

Admin Features
- User history with IP & Date
- Inbuilt pageview and unique visitor counter
- Clean and responsive admin panel
- Admin history loging
- SEO settings such as Site name, Site slogan, Site meta description etc..
- Ads control panel (Ads with prefect spots to increase click's)
- Quick email section to answer the clients.
- Support blocking of specific domain from searching.
- Ban specific user IP from accessing the site
- Create unlimited custom pages (eg.About US, Blog etc�)
- Google analytics support
- Efficient admin panel (Highly customizable)

Demo
http://script3.prothemes.biz/

Admin panel demo
http://script3.prothemes.biz/login.php

Admin username: admin@prothemes.biz
Admin password: password

Note: Some feature are disabled for security reasons. Also site hosted on shared hosting. so don't expect faster load.


REQUIREMENTS
-MySQLI
-PHP 5.3 (or higher)
-PHP CURL() function enabled.
-Mcrypt Extension.
-file_get_contents() must be allowed.
-PHP GD library (required for the PDF generator).
-Higher execution time

 

Easy Installation
- Attractive installer panel, Also only few clicks are needed to install the script.
 

Do you have an idea?
Do you have an idea that you think to improve this item and web application performance
you are most welcome to share your idea.

Version 1.5 (13/11/2014)
- Added: Bing Indexed Pages
- Added: Yahoo Indexed Pages
- Improved site preview algorithm
- Improved domain validator
- Button names fixes!
- Code cleanup and minor bugs fixed.

Version 1.4 (18/9/2014)
- Added: Enter key action to analyze the site
- Improved invalid domain detection
- Removed delicious and added pinterest
- Code cleanup and minor bugs fixed.

Version 1.3 (11/9/2014)
- Added: Server Side domain name validator
- Sub-Domain's no more allowed! (Added Javascript validate function)
- Bug fix: Sitemap errors fixed now
- Bug Fix: Bing backlinks count fixed. 

Version 1.2 (4/8/2014)
- Google Safe Browsing checker.
- Website antivirus checker (Identifying potentially harmful sites)
- Share IT widget on Result page.
- New customized theme with options.
- Optimized index page.
- XML Sitemap generator.
- Fully SEO friendly URL.
- Bug fix: Unable to detect sitemap on "WWW" redirect sites.
- Bug Fix: Footer links not working
- And minor bugs fixed.

Version 1.1 (1/7/2014)
� Fully translatable to any language
- Robots.txt and Sitemap checker included
- gTLD whois support
- Ban specific site from TurboSEO to analyzing
- Ban specific user IP from accessing the site
- New Website Screenshot API
- Create unlimited custom pages (eg.About US, Blog etc�)
- Google analytics support
- Efficient admin panel (Highly customizable)

Version 1.0
- initial release


Contact US: rainbowbalajib@gmail.com