<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse-menu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><i class="icon fa fa-bolt"></i> <?php  
                    if ($site_name=="")
                    echo $msite_name;
                    else
                    echo $site_name;
                    ?></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="collapse-menu">
                    <ul class="nav navbar-nav navbar-right">
                        <li <?php if (strpos($_SERVER['PHP_SELF'],'index.php') !== false)
                        echo 'class="active"';
                        ?>
                        ><a href="index.php"><?php echo $lang['79']; ?></a></li>
                        <li <?php if (strpos($_SERVER['PHP_SELF'],'recent.php') !== false)
                        echo 'class="active"';
                        ?>>
                            <a href="recent.php"><?php echo $lang['80']; ?></a>
                        </li>        
                        <li>
                            <a href="#" data-target="#compose-modal" data-toggle="modal"><?php echo $lang['81']; ?> </a>
                        </li>
                        <?php
    $query =  "SELECT * FROM pages";
    $result = mysqli_query($con,$query);
        
    while($row = mysqli_fetch_array($result)) {
    $page_name =  Trim($row['page_name']);
    $cl = "";
    if (strpos($_SERVER['QUERY_STRING'],$page_name) !== false)
    $cl = "active";
    echo '<li class="'.$cl.'"><a href="pages.php?page='.$page_name.'">'.$page_name.'</a></li>';
    }
                        ?>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container -->
             </nav><!--/.navbar-->